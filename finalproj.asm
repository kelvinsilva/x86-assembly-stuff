TITLE Kelvin Silva Final Project
;NOTE that i follow microsoft calling convention with the exception that I use EAX, EBX, ECX, EDX, ESI, EDI registers.
;Expect return values in EAX. ebx, ecx, edx, esi, edi, expect to be used with each function call.


.386
.model flat, stdcall
option casemap:none
INCLUDE Irvine32.inc
INCLUDELIB kernel32.lib

STD_OUTPUT_HANDLE equ -11
STD_INPUT_HANDLE equ -10
GetStdHandle PROTO, nStdHandle: DWORD
WriteConsoleA PROTO, handle: DWORD, lpBuffer:PTR BYTE, nNumberOfBytesToWrite:DWORD, lpNumberOfBytesWritten:PTR DWORD, lpReserved:DWORD
ExitProcess PROTO, dwExitCode: DWORD

.stack 4096

.data

consoleInHandle DWORD 0
consoleOutHandle DWORD 0

inputBuffer BYTE 255 DUP(0)
inputBufferSize equ ($ - inputBuffer)
bytesRead DWORD 0

bytesWritten DWORD 0

prompt1 BYTE "Enter a number to find its next smallest palindrome", 13, 10
lprompt1 equ ($ - prompt1)

prompt2 BYTE "The next smallest palindrome is: "
nextSmallestPalindromeOuputChar BYTE 255 DUP(0)
lprompt2 equ ($ - prompt2)

numberInput DWORD 0
nextSmallestPalindromeOutput DWORD 0




buffer BYTE 255 DUP(0)


.code

main PROC
	
	INVOKE GetStdHandle, STD_OUTPUT_HANDLE	;get the handle of standard output
	mov consoleOutHandle, eax			;move handle to memory

	INVOKE GetStdHandle, STD_INPUT_HANDLE	;get handle of standard input
	mov consoleInHandle, eax

	mov edx, OFFSET prompt1				;store prompt1 for display	
	mov eax, lprompt1
	INVOKE WriteConsoleA, consoleOutHandle, edx, eax, OFFSET bytesWritten, 0	
	mov eax, OFFSET inputBuffer
	mov edx, OFFSET inputBufferSize
	INVOKE ReadConsole, consoleInHandle, eax, edx, OFFSET bytesRead, 0
	
		;mov edx, OFFSET inputBuffer
		;mov eax, bytesRead
		;INVOKE WriteConsoleA, consoleOutHandle, edx, eax, OFFSET bytesWritten, 0
	
	;------------------------------------
				;Erase carriage return and line feed. Turn the input buffer into properly null terminated string
		push edi
		xor eax, eax
		mov al, 10				;Enter the value to compare the string to
		mov edi, OFFSET inputBuffer	;move the  address of string we are comparing to
		mov ecx, inputBufferSize		
		cld
		repne scasb
		dec edi					;avoid off by 1 error
		mov BYTE PTR [edi], 0		;replace carriage return or line feed with null termination character

		xor eax, eax
		mov al, 13
		mov edi, OFFSET inputBuffer
		mov ecx, inputBufferSize
		cld
		repne scasb
		dec edi
		mov BYTE PTR [edi], 0

		pop edi
	;-----------------------------------------

	sub bytesRead, 2				;subtract bytes read by 2, such that it accounts for carriage return line feed.
	
	mov eax, bytesRead
	push eax
	mov eax, OFFSET inputBuffer
	push eax
	call nextSmallestPalindrome
	
	mov nextSmallestPalindromeOutput, eax
	push OFFSET nextSmallestPalindromeOuputChar
	push OFFSET nextSmallestPalindromeOutput
	call intToChar

	mov edx, OFFSET prompt2				;store prompt1 for display	
	mov eax, lprompt2
	INVOKE WriteConsoleA, consoleOutHandle, edx, eax, OFFSET bytesWritten, 0	
	mov eax, OFFSET inputBuffer
	mov edx, OFFSET inputBufferSize




	;Function call
	;push OFFSET cnum ;Pass right param
	;push OFFSET number ;Pass left param
	;call intToChar

	;Function call
	;push OFFSET dest_num	;pass right param
	;push OFFSET source_num	;pass left param
	;call getReverseStr

	;push OFFSET num
	;push OFFSET numc
	;call charToInt

	;mov edx, OFFSET cnum
	;call WriteString

INVOKE ExitProcess, 0

main ENDP


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Using STDCALL
;Parameters: address of character string of number, size of character string
;Return: ****************next smallest palindrome (DWORD) is saved in EAX as DWORD**********************
;stdcall
nextSmallestPalindrome PROC ;Get next smallest palindrome

	push ebp
	mov ebp, esp
	mov ecx, [ebp + 12]

	;Check for single digit edge case, digits 0 - 8 	
	cmp ecx, 1
	je SINGLEDIGIT

	;Check for all 9 digits edge case.
	mov edi, [ebp + 8]
	mov al, '9'
	;mov ecx, [ebp + 12]
	cld
	repe scasb
	je ALLNINE
	jmp EVENODDSTART

ALLNINE:
		
		push 0
		lea eax, [ebp - 4]
		push eax
		push [ebp + 8]
		call charToInt
		pop eax
		add eax, 2
		push eax
		mov eax, esp


		mov ecx, [ebp + 12]
		add ecx, 2
		sub esp, 1
		mov BYTE PTR [esp], 0

		LLLLL1:
			sub esp, 1
			mov BYTE PTR [esp], 0
		loop LLLLL1
		push esp
		push eax
		call intToChar
		
	jmp EXIT

SINGLEDIGIT:
	
	xor eax, eax
	mov eax, [ebp + 8]
	mov al, BYTE PTR [eax]
	and eax, 000000ffh		;clear big end of eax, ax, and only leave al

	add al, 1
	push 0
	push eax
	jmp EXIT
	
EVENODDSTART:
					;we segregate the palindrome into two cases, even number of digits, and odd number of digits
	xor edx, edx
	mov eax, [ebp + 12]	;construct a qword from eax and edx. ebp+12 is right operand, size of character string
	mov ecx, 2		;prepare for division
	idiv ecx

	cmp edx, 0		;if we have modulus inside edx, then its odd. if edx is 0 then it is even
	je LEVEN
	jmp LODD

	
LEVEN:
	nop

;--------int halfOfLengthOriginal = inputString.length()/2
	mov ecx, [ebp + 12]
	shr ecx, 1	;div by 2
	push ecx		;halfLengthOfOriginal: ebp - 4
	push 0		;make space for dword equivalent of half reverse string this is [ebp - 8]

;----------------------------------------------------------------------
;string reversedLeftHalfStr = get_reverse(leftHalfStr);
	mov edx, esp	;save esp original
	L1:
		sub esp, 1	;make array that is as big as half of input
	loop L1
	mov esp, edx	;restore original esp
	
	mov ecx, [ebp - 4]
	mov eax, [ebp + 8]
	xor edx, edx
	
	sub esp, 1
	mov BYTE PTR [esp], 0	;account for null termination character
	L2:
		mov dl, BYTE PTR [eax]
		sub esp, 1
		mov [esp], dl
		add eax, 1
	loop L2

;--------int reversedLeftHalf = atoi (reversedLeftHalfStr.c_str())

	mov edx, esp
	lea eax, [ebp - 8]
	
	push eax			;right param
	push edx			;left param
	call charToInt		;result is stored in eax, and in memory passed as address, note this is reversed string
;-----------------------------------------------------------------------------
;-----------------------------------------------------------------------------
; string leftHalfStr = inputString.substr(0, halfOfLengthOfOriginal, string::npos)

	push 0			;make int lefthalf = 0
	mov ecx, esp		;save esp

	sub esp, [ebp - 4]	;make another array same size as halflengthorig, to make left half str
	sub esp, 1		;to account for null terminator
	mov eax, esp		;save the destination source array address to eax

	push ecx			;save line 221's esp to stack
	push eax			;push right operand dest str from line 225
		
		mov edx, ebp	;--------------Get address of first character in halflengthorig
		sub edx, 8	;			address of halflengthorig = ebp - 8 - halflengthorig(ebp - 4) - 1
		sub edx, [ebp - 4]	;half length of original
		sub edx, 1		;subtract 1 for null termination
	
	push edx		;push left operand source str
	call getReverseStr

;--------int leftHalf = atoi(rightHalfStr.c_str())

	;reference line 227, an extra parameter is already on the stack 
	;(address of int left half in line 220)
	lea eax, [esp + 4]
	push eax
	call charToInt
;--------------------------------------------
;build right half str
	push 0	;int rightHalf = 0
	mov edx, esp

	mov eax, [ebp + 8]
	
	mov ecx, eax
	add ecx, [ebp - 4]
	sub ecx, 1		;off by one error

	add eax, [ebp - 4]	;add 0th position by halfLengthOriginal ([ebp -4]) to get rightHalf
	add eax, [ebp - 4]	;go to end of string

	xor edx, edx

	;sub esp, 1
	;mov BYTE PTR [esp], 0		;account for null termination character
	L4:						;store righthalf into stack, note that it is reversed. 
							;so we must reverse this to get actual righthalf
		cmp eax, ecx
		je L4EXIT
			sub esp, 1
			mov dl, BYTE PTR [eax]
			mov [esp], dl
			sub eax, 1
	jmp L4
	
L4EXIT:
	
	mov eax, esp		;esp points to beginning of rightHalfstr
	add eax, [ebp - 4]	;move esp back halfStringLength to go back to the numerical equivalent- reference line 254
	add eax, 1
	
	push eax
	mov eax, esp
	add eax, 4
	push eax	
	call charToInt
;----------------------Start comparison
	;eax contains rightHalf integer, ecx contains reversedLeftHalf integer
	mov ecx, [ebp - 8]
	cmp ecx, eax

	ja   LAB_rLH_GT_rH
	jmp	LAB_rLH_GT_rH_ELSE
;--------------------------------
	LAB_rLH_GT_rH: ; if (reversedLeftHalf > rightHalf)

	mov ecx, [ebp + 12]
	sub esp, 1
	mov BYTE PTR [esp], 0

	lea ecx, [ebp - 8]		;point to revlefthalfint
	sub ecx, [ebp - 4]		;point to lefthalfstring
	sub ecx, 1			;sub 1 for null term
	sub ecx, 4			;sub 4 for integer representation 
	sub ecx, [ebp - 4]		;point to leftHalfString
	sub ecx, 1			;account for nullterm

	xor edx, edx
	LLL2:				
		mov dl, BYTE PTR [ecx]
		cmp dl, 0
	je LLL2_END
			sub esp, 1
			mov BYTE PTR [esp], dl
			add ecx, 1	
	jmp LLL2
	LLL2_END:

					;ex. 4321 --> __34 --> 4334
	;load address of reversedLeftHalf str, when popped into stack, becomes left half str
	lea ecx, [ebp - 8]
	sub ecx, [ebp - 4]
	sub ecx, 1

	xor edx, edx
	LLL3:
		mov dl, BYTE PTR [ecx]
		cmp dl, 0
	je LLL3_END
			sub esp, 1
			mov BYTE PTR [esp], dl
			add ecx, 1	
	jmp LLL3
	LLL3_END:
	
	jmp EXIT

	LAB_rLH_GT_rH_ELSE:
		sub esp, 1			;make extra space when we overwrite with get reverse
		mov BYTE PTR [esp], 0
		sub esp, 1			;make extra space when we overwrite with get reverse
		mov BYTE PTR [esp], 0
	
		lea edx, [ebp - 8]		;point to revlefthalfint
		sub edx, [ebp - 4]		;point to lefthalfstring
		sub edx, 1			;sub 1 for null term
		sub edx, 4			;sub 4 for integer representation 
		add DWORD PTR [edx], 1

		mov ecx, [ebp - 4]
		add ecx, 1

		sub esp, 1
		mov BYTE PTR [esp], 0	;account for null termination
		LLL4:
			sub esp, 1
			mov BYTE PTR [esp], 0
		loop LLL4
		
		push esp
		push edx
		call intToChar
		
		mov edx, esp
		mov ecx, 0			;obtain count of size of newReversedLeftHalf
		LLL5:
			mov al, BYTE PTR [edx]
			add edx, 1
			add ecx, 1
			cmp al, 0
		jne LLL5
		sub ecx, 1			;account for off by 1 error

		mov eax, esp
		mov edx, esp
		add edx, ecx
		push edx
		push eax
		call getReverseStr
		
		jmp EXIT


	nop

	jmp EXIT
;********************************************************ODD CASE***************************************
;*******************************************************************************************************
LODD:	;odd number of digits case....
	
	nop

;--------int halfOfLengthOriginal = inputString.length()/2
	mov ecx, [ebp + 12]
	shr ecx, 1	;div by 2
	push ecx	;halfLengthOfOriginal: ebp - 4
	push 0	;make space for dword equivalent of half reverse string this is [ebp - 8]

;----------------------------------------------------------------------
;string reversedLeftHalfStr = get_reverse(leftHalfStr);
	mov edx, esp	;save esp original
	_L1:
		sub esp, 1	;make array that is as big as half of input
	loop _L1
	mov esp, edx	;restore original esp
	
	mov ecx, [ebp - 4]
	mov eax, [ebp + 8]
	xor edx, edx
	
	sub esp, 1
	mov BYTE PTR [esp], 0	;account for null termination character
	_L2:
		mov dl, BYTE PTR [eax]
		sub esp, 1
		mov [esp], dl
		add eax, 1
	loop _L2

;--------int reversedLeftHalf = atoi (reversedLeftHalfStr.c_str())

	mov edx, esp
	lea eax, [ebp - 8]
	
	push eax			;right param
	push edx			;left param
	call charToInt		;result is stored in eax, and in memory passed as address, note this is reversed string
;-----------------------------------------------------------------------------
;-----------------------------------------------------------------------------
; string leftHalfStr = inputString.substr(0, halfOfLengthOfOriginal, string::npos)

	push 0			;make int lefthalf = 0
	mov ecx, esp		;save esp

	sub esp, [ebp - 4]	;make another array same size as halflengthorig, to make left half str
	sub esp, 1		;to account for null terminator
	mov eax, esp		;save the destination source array address to eax

	push ecx			;save line 221's esp to stack
	push eax			;push right operand dest str from line 225
		
		mov edx, ebp	;--------------Get address of first character in halflengthorig
		sub edx, 8	;			address of halflengthorig = ebp - 8 - halflengthorig(ebp - 4) - 1
		sub edx, [ebp - 4]	;half length of original
		sub edx, 1		;subtract 1 for null termination
	
	push edx		;push left operand source str
	call getReverseStr

;--------int leftHalf = atoi(rightHalfStr.c_str())

	;reference line 227, an extra parameter is already on the stack 
	;(address of int left half in line 220)
	lea eax, [esp + 4]
	push eax
	call charToInt



;--------------------------------------------
;build right half str
	push 0	;int rightHalf = 0
	mov edx, esp

	mov eax, [ebp + 8]
	
	mov ecx, eax		;we set eax at the end of the string and walk back to meet ecx
	add ecx, [ebp - 4]	;we want to be at half of the string
	

	add eax, [ebp - 4]	;add 0th position by halfLengthOriginal ([ebp -4]) to get rightHalf
	add eax, [ebp - 4]	;go to end of string

	xor edx, edx

	sub esp, 1
	mov BYTE PTR [esp], 0		;account for null termination character
_L4:					;store righthalf into stack, note that it is reversed. 
					;so we must reverse this to get actual righthalf
	cmp eax, ecx
	je _L4EXIT
		sub esp, 1
		mov dl, BYTE PTR [eax]
		mov [esp], dl
		sub eax, 1
	jmp _L4
	
_L4EXIT:
	
	mov eax, esp		;esp points to beginning of rightHalfstr
	add eax, [ebp - 4]	;move esp back halfStringLength to go back to the numerical equivalent
	add eax, 1
	
	push eax
	mov eax, esp
	add eax, 4
	push eax	
	call charToInt
;------------------------right half str build end-------------------------------------------
	push eax
;-------------get middle char,
	mov eax, [ebp + 8]
	add eax, [ebp - 4]

	xor ebx, ebx
	mov bl, BYTE PTR [eax]
;-------------get middle char
	
	pop eax
	;eax contains rightHalf integer, ecx contains reversedLeftHalf integer
	mov ecx, [ebp - 8]
	cmp ecx, eax

	ja   _LAB_rLH_GT_rH
	jmp	_LAB_rLH_GT_rH_ELSE

	_LAB_rLH_GT_rH: ; if (reversedLeftHalf > rightHalf)

	mov ecx, [ebp + 12]
	sub esp, 1
	mov BYTE PTR [esp], 0

	lea ecx, [ebp - 8]		;point to revlefthalfint
	sub ecx, [ebp - 4]		;point to lefthalfstring
	sub ecx, 1			;sub 1 for null term
	sub ecx, 4			;sub 4 for integer representation 
	sub ecx, [ebp - 4]		;point to leftHalfString
	sub ecx, 1			;account for nullterm

	xor edx, edx
	_LLL2:				
		mov dl, BYTE PTR [ecx]
		cmp dl, 0
	je _LLL2_END
			sub esp, 1
			mov BYTE PTR [esp], dl
			add ecx, 1	
	jmp _LLL2
	_LLL2_END:
	
	;---------------------------------------------
	sub esp, 1			;Add middle character
	mov BYTE PTR[esp], bl	;Add middle character
		
					;ex. 4321 --> __34 --> 4334
	;load address of reversedLeftHalf str, when popped into stack, becomes left half str
	lea ecx, [ebp - 8]
	sub ecx, [ebp - 4]
	sub ecx, 1

	xor edx, edx
	_LLL3:
		mov dl, BYTE PTR [ecx]
		cmp dl, 0
	je _LLL3_END
			sub esp, 1
			mov BYTE PTR [esp], dl
			add ecx, 1	
	jmp _LLL3
	_LLL3_END:
	
	jmp EXIT

_LAB_rLH_GT_rH_ELSE:
		
		cmp ebx, 57			;compare with 9
		je SETMIDZERO
		jmp INCMID
	
	SETMIDZERO:
			mov ebx, 48
	SETMIDZEROEND:
			jmp INCMIDEND
	INCMID:
			add ebx, 1
	INCMIDEND:

		sub esp, 1			;make extra space when we overwrite with get reverse
		mov BYTE PTR [esp], 0
		sub esp, 1			;make extra space when we overwrite with get reverse
		mov BYTE PTR [esp], 0
	
		lea edx, [ebp - 8]		;point to revlefthalfint
		sub edx, [ebp - 4]		;point to lefthalfstring
		sub edx, 1			;sub 1 for null term
		sub edx, 4			;sub 4 for integer representation 
		add DWORD PTR [edx], 1	;leftHalf++

		mov ecx, [ebp - 4]		;Prepare ecx for loop instruction
		add ecx, 1			;ebp is halflengthorig + 1

		sub esp, 1
		mov BYTE PTR [esp], 0	;account for null termination
		_LLL4:
			sub esp, 1
			mov BYTE PTR [esp], 0
		loop _LLL4
		
		push esp
		push edx
		call intToChar			;leftnewHalf is concatenated to string

		
		;prepare for reversedNewLeftHalf concatenation, _LLL5 is to count size of half
		mov edx, esp
		mov ecx, 0			;obtain count of size of newReversedLeftHalf
		_LLL5:
			mov al, BYTE PTR [edx]
			add edx, 1
			add ecx, 1
			cmp al, 0
		jne _LLL5
		sub ecx, 1			;account for off by 1 error
		
		
		

		mov eax, esp	;point to beginning of source str 
		mov edx, esp	;point to beginning of source str

		add edx, ecx		;point to end of source str and into the beginning of dest str
		push edx

		add edx, 1		;point to one ahead for middle character

		push edx
		push eax
		call getReverseStr
		pop edx
		mov BYTE PTR [edx], bl	;insert middle character in space allotted
		
		jmp EXIT


	nop

	jmp EXIT


	
EXIT:
	mov eax, esp
	push 0
	push esp
	push eax
	call charToInt
	;address is stored in eax automatically by call charToInt

	mov esp, ebp
	pop ebp

	ret 8
nextSmallestPalindrome ENDP

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Parameters: address of source string to be reversed (DWORD), address of where to store reversed string  (DWORD)
;Return value: eax writes to zero
;stdcall
getReverseStr PROC ;Get the reverse of a string

	push ebp
	mov ebp, esp

	mov eax, [ebp + 8]
	mov ecx, 0
	xor edx, edx

	L1:
		mov dl, BYTE PTR [eax]
		add eax, 1
		push DWORD PTR edx
	cmp dl, 0
	jnz L1

	pop eax			;pop extra thing from loop we get an extra character that is not supposed to be in stack
	mov eax, [ebp + 12]

	L2:				;Pop everything off the stack to get reversed string
		pop edx
		mov BYTE PTR [eax], dl
		add eax, 1	;move to next character
	cmp ebp, esp		;pop untill all local variables are popped, the only local variables in stack are letters
	jne L2

	;mov edx, OFFSET dest_num	;used for debugging purposes
	;call WriteString

	
	mov esp, ebp
	pop ebp

	ret 8
getReverseStr ENDP


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Paremeters:		address of source integer to be converted DWORD , address of where to store the character array DWORD
;Return value:		Eax is written to zero
;C++ Prototype:	int intToChar(int *sourceInt, char *destChar)
;stdcall - expect eax, ecx, edx to be modified. all other registers are unused.
intToChar PROC ;Convert integer to character array

	push ebp
	mov ebp, esp

	mov eax, [ebp + 8]		;move the first parameter sourceInt into eax
	mov eax, [eax]			;dereference eax
	xor edx, edx
	mov  ecx, 10
	
	push 0
	L1:

		idiv  ecx
		push edx
		add  DWORD PTR [ebp - 4], 1
		xor edx, edx

	cmp eax, 0
	jnz L1

	mov ecx, [ebp - 4]
	mov eax, DWORD PTR [ebp + 12]
	xor edx, edx
	L2:
		pop edx
		add edx, 48
		mov [eax], dl
		add eax, 1
		
	loop L2
	
	;mov edx, DWORD PTR [ebp + 12] ;used for debugging
	;call WriteString			 ;used for debugging

	xor eax, eax

	add esp, 4				;pop last thing from stack

	mov esp, ebp
	pop ebp

	ret 8

intToChar ENDP
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Parameters: address of source character array to be converted (DWORD), address of where to store integer value (DWORD) (make sure int value is set to zero)
;Return value: EAX is written to integer value.
;stdcall
;Is dependent on getReverseStr
charToInt PROC ;Convert character to integer value

	push ebp
	mov ebp, esp
	
	mov eax, [ebp + 8]	;address of source character array (param 1)
	xor edx, edx		
	xor ecx, ecx
 
	L1:                                      ;Create x amount of local variables, x being the size of source array
          mov dl, BYTE PTR [eax]
          add eax, 1
          sub esp, 1
          mov BYTE PTR [esp], 0
     
     cmp dl, 0
     jne L1
           
     sub eax, [ebp + 8]
           
           
     push esp			
	push [ebp + 8]
     call getReverseStr ;Function call to place the reverse of source array into stack
	
	mov edx, esp		;save esp ptr
	xor ebx, ebx
	L2:			    ;subtract 48 from each value to obtain integral value.
	
		cmp BYTE PTR [esp], 0
		je L2EXIT

		sub BYTE PTR [esp], 48
		add esp, 1
		add ebx, 1
	jmp L2
	L2EXIT:

	mov esp, edx		;move esp back to original position
	xor eax, eax		;prepare for next loop
	mov edx, 10
	mov ecx, 0

	L3:
		xor eax, eax
		mov al, BYTE PTR [esp]
		
		push ecx
		LL1:
			cmp ecx, 0
			je LL1OUT

			imul eax, edx
			sub ecx, 1
			jmp LL1
		LL1OUT:
		pop ecx

		push ecx
			mov ecx, [ebp+12]
			add [ecx], eax
		pop ecx

		add esp, 1
		add ecx, 1
		sub ebx, 1

	cmp ebx, 0
	jne L3

	mov eax, [ebp + 12]		
	mov eax, [eax]		;set the return value in eax
	;call WriteInt ;used for debugging purposes

	mov esp, ebp
	pop ebp

	ret 8

charToInt ENDP

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

END main




