Kelvin da Silva

Project Report

	Description

	The program takes in an integer value such as 1234 and determines the next closest integer value that is a palindrome. I used win32 api functions to read and write to console. Only macro used is PROC, PROTO, and INVOKE. No macros used. Program structure:
	
	main: handles user input and output, calls nextSmallestPalindrome

	nextSmallestPalindrome: finds next smallest palindrome, reliant on charToInt, intToChar, getReverseStr

	charToInt: converts character array to integer value
	
	intToChar: converts integer dword into character array

	getReverseStr: reverse contents of string pointed to by memory address

I used microsoft calling convention because I think its a better modular design when the caller does not have to do any cleanup. 
The calling convention states that EAX, ECX, EDX may be used as registers, and return value is in EAX, but instead I used all registers and stored return values in EAX.


The algorithm works as follows:
For even number of digits in number:

split the number in half, the left half assign to x, and right half assign to y, for example 1234:
x: 12
y: 34
then take x and reverse it.

If the reverse of x is greater than the integral value of y, then make a new empty string z and append x.
then reverse of x to z.

ex, 4321 -> 4334
if y is of greater integral value than the reverse of x, then increment x by 1 and append to new string z, x. Then append the reverse of x after it has been incremented.
Ex, 1234 -> 1331,

For odd number of digits in input:
example, 12456
x: 4
y: 12
w: 56

assign middle digit to x, left half to y, and right half to w

if the reverse of y has a greater integral value than w,
	then append y to new string z. append x to z. append the reverse of y to z.
else if w is greater than greater than or equal to the integral value of reverse of y,
	and x is equal to 9
		increment y by 1, append y to new string z, append 0 to new string z (as the middle digit), and append the reverse of y to z.
	if x is not 9, 
		then append y to new string z, increment x by 1 and append to z, append the reverse of y to z.
end

For example:
input: 4321
output: 4334

Input: 1234
Output: 1331

Input: 31
Ouput: 33

Input: 1
Output: 2

(This is an edge case, for all numbers 1-8)

Input: 999 (also an easy case, just add 2 to get next palindrome if all numbers are 9)
Output: 1001

Input: 12345
Output: 12421

Input: 54321
Output: 54345

	Difficulties Encountered

	It was difficult getting accustomed to using the stack and acknowledging the fact that the stack grows down. I used the debugger a lot to analyze esp and ebp, and to see if everything was working correctly. I had a lot of problem with working on a 800line program on a wide screen monitor, and I felt like I couldn't see enough code on the screen. It was like looking at code through a pair of magnifying glasses. I think next time I will make my monitor into portrait mode so I can read a large chunk of code at a time instead of having to scroll around in visual studio. Also since assembly is so detailed, writing my project was very frustrating and I almost gave up, but it worked out in the end. I am so thankful for c/c++.

	Estimate of Hours: 30-40 hrs

	Known Bugs or Flaws

	Large integer values (exceeding 11 digits) won't work to find the next smallest palindrome. Negative numbers do ont work, I restricted the program so that it only uses positive numbers. Negative numbers used to quit. No sanitization of user input. 
